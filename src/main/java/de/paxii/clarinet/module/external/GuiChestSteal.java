package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.IInventory;

import java.io.IOException;
import net.minecraft.item.ItemAir;
import net.minecraft.item.ItemStack;

public class GuiChestSteal extends GuiChest {

  private IInventory lowerInv;
  private IInventory upperInv;
  private int delay;

  GuiChestSteal(IInventory upperInv, IInventory lowerInv, int delay) {
    super(upperInv, lowerInv);
    this.upperInv = lowerInv;
    this.lowerInv = upperInv;
    this.delay = delay;
  }

  @Override
  public void initGui() {
    super.initGui();
    int posY = (this.height - this.ySize) / 2 + 3;
    this.buttonList.add(new GuiButton(1, this.width / 2 - 5, posY, 40, 12, "Steal"));
    this.buttonList.add(new GuiButton(2, this.width / 2 + 40, posY, 40, 12, "Store"));
  }

  @Override
  protected void actionPerformed(GuiButton button) throws IOException {
    if (button.id == 1) {
      this.stealInventory();
    } else if (button.id == 2) {
      this.storeInventory();
    }
  }

  private void stealInventory() {
    new Thread(() -> {
      try {
        for (int i = 0; i < this.upperInv.getSizeInventory(); i++) {
          if (this.skipItem(this.upperInv, i)) {
            continue;
          }
          Wrapper.getMinecraft().playerController.windowClick(
              this.inventorySlots.windowId,
              i,
              0,
              ClickType.QUICK_MOVE,
              Wrapper.getPlayer()
          );

          try {
            Thread.sleep(this.delay);
          } catch (InterruptedException ignored) {
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }).start();
  }

  private void storeInventory() {
    new Thread(() -> {
      try {
        int lowerInv = this.lowerInv.getSizeInventory() - 5;
        int upperInv = this.upperInv.getSizeInventory();
        for (int i = 0; i < lowerInv; i++) {
          int slotID;
          if (i > 9) {
            slotID = upperInv + (lowerInv - i) - 1;
          } else {
            slotID = (upperInv + lowerInv - 9) + i;
          }
          if (this.skipItem(this.lowerInv, i)) {
            continue;
          }

          Wrapper.getMinecraft().playerController.windowClick(
              this.inventorySlots.windowId,
              slotID,
              0,
              ClickType.QUICK_MOVE,
              Wrapper.getPlayer()
          );

          try {
            Thread.sleep(this.delay);
          } catch (InterruptedException ignored) {
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }).start();
  }

  private boolean skipItem(IInventory inventory, int slot) {
    ItemStack currentStack = inventory.getStackInSlot(slot);

    return currentStack.getItem() instanceof ItemAir;
  }
}
