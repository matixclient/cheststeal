package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.gui.DisplayGuiScreenEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.module.settings.ValueBase;

import java.util.HashMap;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.inventory.IInventory;

import java.lang.reflect.Field;

public class ModuleChestSteal extends Module {

  private static final String FIELD_UPPER = "upperChestInventory";
  private static final String FIELD_LOWER = "lowerChestInventory";

  public ModuleChestSteal() {
    super("ChestSteal", ModuleCategory.OTHER);

    this.setVersion("1.0");
    this.setBuildVersion(18100);
    this.setDescription("Allows you to steal items from chests using a button");
    this.setRegistered(true);
    this.setEnabled(true);

    this.getModuleValues().put("moveDelay", new ValueBase("ChestSteal Delay", 50.0F, 0.0F, 250.0F));
  }

  @EventHandler
  public void onDisplayGui(DisplayGuiScreenEvent event) {
    if (event.getGuiScreen() instanceof GuiChest &&
        !(event.getGuiScreen() instanceof GuiChestSteal)) {
      try {
        HashMap<String, IInventory> invMap = new HashMap<>();
        GuiChest chest = (GuiChest) event.getGuiScreen();

        for (String fieldName : new String[]{FIELD_UPPER, FIELD_LOWER}) {
          Field inventoryField = GuiChest.class.getDeclaredField(fieldName);
          inventoryField.setAccessible(true);
          IInventory inventory = (IInventory) inventoryField.get(chest);
          invMap.put(fieldName, inventory);
        }

        event.setGuiScreen(new GuiChestSteal(
            invMap.get(FIELD_UPPER),
            invMap.get(FIELD_LOWER),
            (int) this.getModuleValues().get("moveDelay").getValue()
        ));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

}
